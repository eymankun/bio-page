---
title: 'Resume'
date: 2023-11-05T15:42:55+08:00
draft: false

showDate: false
showDateUpdated: false
showHeadingAnchors: false
showPagination: false
showReadingTime: false
showTableOfContents: true
showTaxonomies: false 
showWordCount: false
showSummary: false
sharingLinks: false
showEdit: false
showViews: true
showLikes: false
layoutBackgroundHeaderSpace: false
showAuthor: false
---
<!-- markdownlint-disable MD033 -->

## Professional Experience

{{<timeline>}}

    {{< timelineItem icon="briefcase-solid" header="DXC Technology" badge="Nov 2019 - Present" subheader="Infra Service Assoc Manager" >}}
    Location: Petaling Jaya, Malaysia <br>
    Managing and operates client data centers.
    {{< /timelineItem >}}

    {{< timelineItem icon="briefcase-solid" header="NTT Data" badge="Feb 2018 - Nov 2019" subheader="System Support Lead" >}}
    Cyberjaya, Malaysia <br>
    Operates and monitor client data centers.
    {{< /timelineItem >}}

    {{< timelineItem icon="briefcase-solid" header="Hitachi Solutions Ltd" badge="April 2016 - Feb 2018" subheader="System Engineer" >}}
    Tokyo, Japan <br>
    Technical support for network appliences.
    {{< /timelineItem >}}

{{</timeline>}}

## Education

{{<timeline>}}

    {{< timelineItem icon="graduation-cap" header="Yokohama National University" badge="2016" subheader="BSc, Electronics and Computer Science" >}}
    Location: Yokohama, Japan
    {{< /timelineItem >}}

    {{< timelineItem icon="graduation-cap" header="University of Malaya " badge="2011" subheader="Japanese Language Foundation Program" >}}
    Location: Kuala Lumpur, Malaysia
    {{< /timelineItem >}}

{{</timeline>}}
