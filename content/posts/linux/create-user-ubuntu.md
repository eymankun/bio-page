---
title: 'How to Create New User in Ubuntu'
date: 2023-12-15T18:42:14+08:00
draft: false
tags: ['linux', 'tutorial']
---

To add new user,

```bash
sudo adduser username
```

For example, to add a new user named `eymankun`, simply run:

```bash
sudo adduser eymankun
```

{{<alert>}}
**NOTE:** You will be prompted to provide password and other details.
{{</alert>}}

Once created, use below command to find out details about created user:

```bash
id eymankun
```

This command will display UID, GID, and group that user belong to.

### Granting new user administrative privilage

To grant the new created user administrative privilage, simply run below command:

```bash
sudo usermod -aG sudo eymankun
```

This command will include username `eymankun` in the `sudo` group.

Use `id` command to check the group user belong to.
