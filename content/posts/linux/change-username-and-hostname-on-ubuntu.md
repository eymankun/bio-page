---
title: 'Change Username and Hostname on Ubuntu'
date: 2024-07-05T00:32:35+08:00
draft: false
tags: ['linux', 'ubuntu', 'tutorial']
---

<!-- # Change Username and Hostname on Ubuntu -->

## Sources

- [https://www.hepeng.me/changing-username-and-hostname-on-ubuntu/](https://www.hepeng.me/changing-username-and-hostname-on-ubuntu/)
- [https://askubuntu.com/questions/34074/how-do-i-change-my-username](https://askubuntu.com/questions/34074/how-do-i-change-my-username)

## Enable and disable root account

1. Enable root account

   ```bash
   sudo passwd root
   ```

2. Disabling (lock) root account

   ```bash
   sudo passwd -l root
   ```

## Changing username

> All these command need to be execute as root.

### Steps

1. ssh to root account

   ```bash
   ssh root@server-ip
   ```

2. change username

   ```bash
   usermod -l new-name -d /home/new-name -m oldname
   ```

3. change group name

   ```bash
   groupmod -n newgroup oldgroup
   ```

4. lock root account

   ```bash
   passwd -l root
   ```

5. logout and re-login with new username

## Changing hostname

### Step 1

#### Option 1

edit `/etc/hostname` file and change current hostname to new hostname

```bash
sudo nano /etc/hostname
```

#### Option 2

Use `hostnamectl` command

```bash
sudo hostnamectl set-hostname new-hostname
```

### Step 2

Edit `/etc/hosts`

```bash
sudo nano /etc/hosts
```

Replace any occurrence of the existing computer name with new one

### Step 3

reboot system

```bash
sudo reboot
```
