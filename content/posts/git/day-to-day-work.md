---
title: 'Day to Day Work'
date: 2023-12-17T17:07:08+08:00
draft: false
tags: ['git', 'status', 'commit', 'stash', 'diff']
# Delete below if not applicable
series: ['Git Notes']
series_order: 3
---

## Day to day git command

### Display status of working directory.

```bash
git status
```

{{<alert>}}
**NOTE:** Options include new, staged, and modified files. It will retrieved branch name, current commit id and changes pending commit
{{</alert>}}

### Add file to staging area

```bash
git add .
```

{{<alert>}}
**NOTE:** If only want to stage a specific file, use `git add <filename>`
{{</alert>}}

### Show changes between working directory and staging area

```bash
git diff [filename] # filename is optional
```

### Show changes between staging and repo

```bash
git fill --staged [filename] # filename is optional
```

### Discard any changes in working direcotory

```bash
git checkout -- [filename]
```

{{<alert>}}
**WARNING!** This operation is unrecoverable!
{{</alert>}}

### Revert repo to a previous know working state

```bash
git reset [filename]
```

## Create a new commit

```bash
git commit -m "your message here"
```

### Remove file from working directory and staging area

```bash
gti rm [filename]
```

## Git Stash

### Put current changes in working directory into stash for later use

```bash
git stash
```

### Apply stored stash content into working directory, and clear stash

```bash
git stash pop
```

### Delete a specific stash from all previous stashes

```bash
git stash drop
```
