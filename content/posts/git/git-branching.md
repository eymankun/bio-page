---
title: 'Git Branching'
date: 2023-12-28T15:57:21+08:00
draft: false
tags: ['git', 'branching']
# Delete below if not applicable
series: ['Git Notes']
series_order: 4
---

List of what command can be use for git branching.

## List all branch

```bash
git branch -a
```

## Create a new branch

```bash
git branch <branch name>
```

## Switch working directory to specified branch

```bash
git branch -b <branch-name>
```

> with `-b`: git will create the specified branch if it does not exist.

## Join specified `from-name` branch into current branch

```bash
git merge from-name
```

## Remove selected branch (if it's already merged into any other)

```bash
git branch -d <branch-name>
```

> Use `-D` for force deletion
