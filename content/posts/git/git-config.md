---
title: 'Git Configuration'
date: 2023-11-10T01:07:12+08:00
draft: false
tags: ['git', 'config']
# Delete below if not applicable
series: ['Git Notes']
series_order: 1
---

To set name that will be attached to commits and tags,

```bash
git config --global user.name "John Doe"
```

Set email that will be attached to commits and tags,

```bash
git config --global user.email "example@email.com"
```

Enable some color of git input

```bash
git config --global color.ui auto
```
