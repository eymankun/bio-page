---
title: "Reviewing Your Git History: A Playful Guide"
date: 2024-08-25T19:37:29+08:00
draft: false
tags: ['git', 'log', 'reflog']
# Delete below if not applicable
series: ['Git Notes']
series_order: 7
---

Hey there, Git enthusiasts! 🌟
Ready to dive into the world of commit history and reference labels? Let’s make this journey fun and easy!

## Listing Commit History of the Current Branch

First up, we have the trusty `git log` command.
Want to see the last few commits? Just add `-n` count to limit the list to the last `n` commits.
It’s like peeking into your recent past!

    git log [-n count]

## Overview of Reference Labels and History Graph

Next, let’s get a bird’s-eye view of your project with a neat, decorated graph.
The `--oneline`, `--graph`, and `--decorate` options make your commit history look like a piece of art. 🎨

    git log --oneline --graph --decorate

## Listing Commits Present on the Current Branch but Not Merged into a Reference

Ever wondered which commits are exclusive to your current branch? Use `git log ref..` to find out.
Here, `ref` can be a branch or a tag name. It’s like finding hidden treasures in your code!

    git log ref..

## Listing Commits Present on a Reference but Not Merged with the Current Branch

Flip the script with `git log ..ref` to see commits that are on ref but not on your current branch.
It’s like checking out what your neighbors have that you don’t!

    git log ..ref

## Listing Operations Made on the Local Repo

Finally, let’s take a trip down memory lane with `git reflog`.
This command shows all the operations (like checkouts or commits) made on your local repo.
It’s your personal Git diary! 📖

    git reflog

And there you have it! A playful guide to reviewing your Git history. Happy coding! 🚀
