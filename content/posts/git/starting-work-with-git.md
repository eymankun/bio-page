---
title: "Starting Work With Git"
date: 2024-08-12T11:48:32+08:00
draft: false
tags: ['git', 'init']
# Delete below if not applicable
series: ['Git Notes']
series_order: 5
---


## Creating an new repo

When creating a new project, use below command in terminal to initialize git.

```zfs
git init [project name]
```

> Note
> If `project name` is provided, git will create a new directory and initialize a repository inside it. <br>
> If not, current directory will be initialize.

## Download a project with entire history from remote repo

```bash
git clone <project url>
```
