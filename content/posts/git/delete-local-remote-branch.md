---
title: "Delete Local Remote Branch"
date: 2024-08-14T12:55:42+08:00
draft: false
tags: ['git', 'branch', 'push']
# Delete below if not applicable
series: ['Git Notes']
series_order: 6
---

## How to delete local branch and remote branch

Managing branches in Git is a common task for developers.
Knowing how to delete both local and remote branches can help your repository clean and organized.

### Deleting local branch

Before deleting a local branch, ensure to switch to a different branch to avoid any issues.
You can do this with the following command.

    git checkout main

Once you are on a different branch, you can delete the target local branch using:

    git branch -d local-branch-name

If the branch has not been merged, you might need to force delete using:

    git branch -D local-branch-name

### Deleting remote branch

Deleting a remote branch need a different approch.
You need to push a delete request to the remote repository.
Use the following command to delete a remote branch:

    git push origin --delete remote-branch-name

Alternatively, you can use the older syntax:

    git push origin :remote-branch-name

### Additional tips

- **Check branches**: Before deleting, you might want to list all branches first to ensure you are deleting the correct target branch.
Use `git branch` for local branches, and `git branch -r` for remote branches.
- **Confirm deletion**: After deleting a branch, confirm its deletion by listing the branch again.

By following these steps, you can efficiently manage Git branches, keeping your workflow smooth and tidy repository.

Source: [freeCodeCamp](https://www.freecodecamp.org/news/how-to-delete-a-git-branch-both-locally-and-remotely/)
