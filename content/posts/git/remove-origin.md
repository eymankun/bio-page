---
title: 'Remove Origin'
date: 2023-11-11T00:40:14+08:00
draft: false
tags: ['git', 'remote', 'origin']
# Delete below if not applicable
series: ['Git Notes']
series_order: 2
---

Removing and adding a new remote origin can be done by below <cite>commands</cite>[^1].

[^1]: source: [Atlassian](https://www.atlassian.com/git/tutorials/syncing)

1. Removing remote

```bash
git remote rm origin
```

2. Check remote URL

```bash
git remote -v
```

3. Adding new remote

```bash
git remote add <name> <url>
```

{{< alert >}}
Note: Name for remote can be anything, but usually use "origin"

{{</ alert >}}

Alternatively, instead of removing and re-adding, do <cite>this</cite> [^2]:

[^2]: source: [Stack Overflow](https://stackoverflow.com/questions/16330404/how-to-remove-remote-origin-from-a-git-repository)

```bash
git remote set-url origin git://new.url.here
```
