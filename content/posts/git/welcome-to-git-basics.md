---
title: "Welcome to Git Basics"
date: 2024-08-12T12:12:10+08:00
draft: true
tags: ['git', 'command', 'basic']
# Delete below if not applicable
series: ['Git Notes']
series_order: 0
---

Hello there! Welcome to my notes on how to use git.
