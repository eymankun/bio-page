#!/bin/bash
git fetch --tags
current_version=$(git describe --tags --abbrev=0 | sed 's/^v//')
# echo "Current version: $current_version"
IFS='.' read -r -a version_parts <<< "${current_version}"
major=${version_parts[0]}
minor=${version_parts[1]}
patch=${version_parts[2]}
patch=$((patch + 1))
new_version="$major.$minor.$patch"
echo $new_version