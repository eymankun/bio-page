# [Personal Website](https://eymankun.gitlab.io/bio-page)

[![pipeline status](https://gitlab.com/eymankun/bio-page/badges/main/pipeline.svg)](https://gitlab.com/eymankun/bio-page/-/commits/main)

Repo for my personal homepage with my resume and a little bit of portfolio (both is still WIP though). This page is also the place to write on what I am interested in.

This website is powered by [Blowfish](https://nunocoracao.github.io/blowfish/)

<details>

- [Tools and Miscs](#tools-and-miscs)
  - [Hugo - Framework](#hugo---framework)
  - [Blowfish - Theme](#blowfish---theme)
- [Updating Blowfish submodule](#updating-blowfish-submodule)
  - [Git submodule](#git-submodule)
  - [Hugo module](#hugo-module)
  - [Manual file copy](#manual-file-copy)

</details>

## Tools and Miscs

These are the tools that I've use to make this homepage.

### Hugo - Framework

<https://gohugo.io>

### Blowfish - Theme

<https://nunocoracao.github.io/blowfish/>

## Updating Blowfish submodule

There are three method to update theme files for this website.

- [Git submodule](#git-submodule)(recommended)
- [Hugo module](#hugo-module)
- [Manual file copy](#manual-file-copy)

### Git submodule

Simply execute below command to update to the latest version of theme.

    git submodule update --remote --merge

Once submodule has been updated, rebuild site and check everything works as expected.

### Hugo module

Simply change into your project directory and execute the following command:

    hugo mod get -u

Hugo will automatically update any modules that are required for your project. It does this by inspecting your `module.toml` and `go.mod` files. If you have any issues with the update, check to ensure these files are still configured correctly.

Then simply rebuild your site and check everything works as expected.

### Manual file copy

Updating Blowfish manually requires you to download the latest copy of the theme and replace the old version in your project.

> Note that any local customisations you have made to the theme files will be lost during this process.
Download the latest release of the theme source code.

[Download from Github](https://github.com/nunocoracao/blowfish/releases/latest)

Extract the archive, rename the folder to blowfish and move it to the themes/ directory inside your Hugo project’s root folder. You will need to overwrite the existing directory to replace all the theme files.

Rebuild your site and check everything works as expected.
